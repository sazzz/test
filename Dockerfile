FROM openjdk:11

WORKDIR /app

COPY target/helloworld.jar /app

CMD ["java", "-jar", "helloworld.jar"]
